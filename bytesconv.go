package bytesconv

import (
	"unsafe"
)

type Bytes struct {
	ptr      unsafe.Pointer
	len, cap int
}

func From[T any](item T) []byte {
	szX := unsafe.Sizeof(item)
	sz := *(*int)(unsafe.Pointer(&szX))
	bytes := Bytes{unsafe.Pointer(&item), sz, sz}
	return *(*[]byte)(unsafe.Pointer(&bytes))
}
func To[T any](bs []byte) T {
	return *(*T)(unsafe.Pointer((*Bytes)(unsafe.Pointer(&bs)).ptr))
}
func FromMeta(in []byte) []byte {
	if len(in) > 24 {
		return nil
	}
	var a [24]byte
	copy(a[:], in)
	return *(*[]byte)(unsafe.Pointer(&a))
}
func ToMeta(in []byte) []byte {
	a := *(*[24]byte)(unsafe.Pointer(&in))
	return a[:]
}
