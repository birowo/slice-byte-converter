package bytesconv

import (
	"fmt"
	"testing"
)

func compare(expected, got string, t *testing.T) {
	t.Helper()
	if expected != got {
		t.Errorf("expexted: %s, got %s\n", expected, got)
	}
}

type Typ [2]struct {
	c [8]byte
	d uint64
}

var (
	a = Typ{
		{[8]byte{0x41, 0x42, 0x43, 0x44, 0x41, 0x42, 0x43, 0x44},
			0x696a6b6c6d6e6f70},
		{[8]byte{0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70},
			0x4142434441424344},
	}
	b []byte
)

func TestFrom(t *testing.T) {
	expected := "4142434441424344706f6e6d6c6b6a69696a6b6c6d6e6f704443424144434241"
	b = From(a)
	got := fmt.Sprintf("%x", b)
	compare(expected, got, t)
}
func TestTo(t *testing.T) {
	expected := fmt.Sprint(a)
	got := fmt.Sprint(To[Typ](b))
	compare(expected, got, t)
}
func TestFromMeta(t *testing.T) {
	type Typ struct {
		x []byte
	}
	expected := "abc"
	a := Typ{[]byte(expected)}
	b := From(a)
	got := string(FromMeta(b))
	compare(expected, got, t)
}
func TestToMeta(t *testing.T) {
	a := []byte("abcde")
	b := ToMeta(a)
	if byte(len(a)) != b[8] || byte(len(a)) != b[16] {
		t.Error("error\n")
	}
}
